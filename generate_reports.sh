#!/bin/bash


if [ "$JACOCO_LIB" = "" ]; then
  JACOCO_LIB="$HOME"
fi
if [ "$CLONE_HOME" = "" ]; then
  CLONE_HOME="$HOME/git"
fi

DIRNAME=`dirname "$0"`
if [ "$DIRNAME" = "." ]; then
  DIRNAME="$PWD"
fi

if [ "$REPORT_DATA" = "" ]; then
  REPORT_DATA="${DIRNAME}/report_data"
fi


mkdir -p "${DIRNAME}/target"
mkdir -p "${REPORT_DATA}"


PROJECT=""
OPERATION=""
CLASSFILES=()
SOURCEFILES=()
EXECFILES=()
COMMITID=""
while IFS= read -r line; do
  FC="${line:0:1}"
  if [ "$FC" = $'\t' ]; then
    if [ "${line:1:1}" != $'\t' ]; then
      OPERATION=""
    fi
    case $OPERATION in
      "")
        OPERATION="${line:1}"
        ;;
      git)
        if [ "${line:2:10}" = "rcommitid:" ]; then
          rsync "${line:12}" "${REPORT_DATA}/${PROJECT}.rcommitid"
          COMMITID=$(cat "${REPORT_DATA}/${PROJECT}.rcommitid")
        elif [ "${line:2:10}" = "lcommitid:" ]; then
          COMMITID=$(cat "${REPORT_DATA}/${line:12}")
        elif [ "${line:2:12}" = "rjacocoexec:" ]; then
          rsync "${line:14}" "${REPORT_DATA}/${PROJECT}-jacoco.exec"
          EXECFILES=("${EXECFILES[@]}" "${REPORT_DATA}/${PROJECT}-jacoco.exec")
        elif [ "${line:2:12}" = "ljacocoexec:" ]; then
          EXECFILES=("${EXECFILES[@]}" "${REPORT_DATA}/${line:14}")
        elif [ "${line:2:6}" = "clone:" ]; then
          GIT_CLONE="${line:8}"
          cd "$CLONE_HOME/$GIT_CLONE"
          git fetch
          git checkout $COMMITID
          cd -
          SOURCEFILES=("${SOURCEFILES[@]}" $(find "$CLONE_HOME/$GIT_CLONE" -name java -type d | grep 'src/main/java$'))
        fi
        ;;
      class)
        if [ "${line:2:6}" = "rarch:" ]; then
          rsync "${line:8}" "${REPORT_DATA}/${PROJECT}.arch"
          CLASSFILES=("${CLASSFILES[@]}" "${REPORT_DATA}/${PROJECT}.arch")
        elif [ "${line:2:6}" = "larch:" ]; then
          CLASSFILES=("${CLASSFILES[@]}" "${REPORT_DATA}/${line:8}")
        fi
        ;;
      *)
        echo "Error OPERATION $OPERATION unknown"
        exit 1
        ;;
    esac
  else
    if [ "$PROJECT" != "" ]; then
      ARGS=(-jar "$JACOCO_LIB/jacococli.jar" report)
      for CLASSFILE in "${CLASSFILES[@]}"; do
        ARGS=("${ARGS[@]}" --classfiles "$CLASSFILE")
      done
      for SOURCEFILE in "${SOURCEFILES[@]}"; do
        ARGS=("${ARGS[@]}" --sourcefiles "$SOURCEFILE")
      done
      java "${ARGS[@]}" --html "${DIRNAME}/target/${PROJECT}-jacoco_site" --xml "${DIRNAME}/target/${PROJECT}-jacoco.xml" "$@"
      if [ "${#EXECFILES[@]}" -ne 0 ]; then
        java "${ARGS[@]}" --html "${DIRNAME}/target/${PROJECT}-jacoco-with-ut_site" --xml "${DIRNAME}/target/${PROJECT}-jacoco-with-ut.xml" "$@" "${EXECFILES[@]}"
      fi

      mkdir -p "${DIRNAME}/target/${PROJECT}-diff-cover_site"
      cd $CLONE_HOME/$GIT_CLONE
      diff-cover --html-report "${DIRNAME}/target/${PROJECT}-diff-cover_site/diff-cover.html" --src-roots $(find . -name java -type d | grep 'src/main/java$') -- "${DIRNAME}/target/${PROJECT}-jacoco.xml"
      if [ "${#EXECFILES[@]}" -ne 0 ]; then
        mkdir -p "${DIRNAME}/target/${PROJECT}-diff-cover-with-ut_site"
        diff-cover --html-report "${DIRNAME}/target/${PROJECT}-diff-cover-with-ut_site/diff-cover.html" --src-roots $(find . -name java -type d | grep 'src/main/java$') -- "${DIRNAME}/target/${PROJECT}-jacoco-with-ut.xml"
      fi
      cd -
    fi
    PROJECT="$line"
    CLASSFILES=()
    SOURCEFILES=()
    EXECFILES=()
    COMMITID=""
  fi
done < <(printf "\n\n" | cat "$DIRNAME/projects.txt" -)
