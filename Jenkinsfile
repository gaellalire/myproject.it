pipeline {
    agent any
    environment {
        JACOCO_LIB = """${sh (
            script: 'if [ "$JACOCO_LIB" = "" ]; then echo $HOME; else echo $JACOCO_LIB; fi',
            returnStdout: true
        ).trim()}"""
    }
    stages {
        stage ('Test') {
            steps {
                wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                    sh 'java -jar $JACOCO_LIB/jacococli.jar dump --address localhost --port 12346 --destfile /dev/null --reset'
                    sh 'mvn -Dmaven.test.failure.ignore=true clean verify'
                    sh 'java -jar $JACOCO_LIB/jacococli.jar dump --address localhost --port 12346 --destfile target/jacoco.exec'
                    sh './generate_reports.sh target/jacoco.exec'
                }
            }
            post {
                always {
                    publishHTML (target: [
                        allowMissing: false,
                        alwaysLinkToLastBuild: false,
                        keepAll: true,
                        reportDir: 'target/myproject-jacoco_site',
                        reportFiles: 'index.html',
                        reportName: "Jacoco IT Report"
                    ])
                    publishHTML (target: [
                        allowMissing: false,
                        alwaysLinkToLastBuild: false,
                        keepAll: true,
                        reportDir: 'target/myproject-diff-cover_site',
                        reportFiles: 'diff-cover.html',
                        reportName: "Jacoco IT Diff Report"
                    ])
                    publishHTML (target: [
                        allowMissing: false,
                        alwaysLinkToLastBuild: false,
                        keepAll: true,
                        reportDir: 'target/myproject-jacoco-with-ut_site',
                        reportFiles: 'index.html',
                        reportName: "Jacoco UT & IT Report"
                    ])
                    publishHTML (target: [
                        allowMissing: false,
                        alwaysLinkToLastBuild: false,
                        keepAll: true,
                        reportDir: 'target/myproject-diff-cover-with-ut_site',
                        reportFiles: 'diff-cover.html',
                        reportName: "Jacoco UT & IT Diff Report"
                    ])
                    junit '**/surefire-reports/**/*.xml'
                }
            }
        }
    }
}

